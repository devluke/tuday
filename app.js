const express = require("express");
const session = require("express-session");
const dateFormat = require("dateformat");
const bodyParser = require("body-parser");
const Datastore = require("nedb");
const flash = require("express-flash-messages");

const { check, validationResult } = require("express-validator/check");

const users = new Datastore({ filename: "./db/users.db", autoload: true });
const events = new Datastore({ filename: "./db/events.db", autoload: true });
const views = new Datastore({ filename: "./db/views.db", autoload: true });

const app = express();

app.set("view engine", "pug");
app.set("views", "./views");

app.use(express.static("public"));

app.use(session({secret: "chqbmqOQKXrHstM604oBOZ3TGCJi9B2U"}));

app.use(bodyParser());

app.use(flash());

app.get("/", (req, res) => {

	const now = Date();

	res.render("index", { title: "Your View", dateFormat: dateFormat, now: now });

});

app.get("/login", (req, res) => {

	res.render("login", { title: "Login" });

});

app.get("/register", (req, res) => {

	res.render("register", { title: "Register" });

});

app.post("/login", (req, res) => {

	console.log(req.body);

});

app.post("/register", [

	check("name")

		.not().isEmpty({ ignore_whitespace: true }).withMessage("Please enter a name")
		.trim()
		.isLength({ min: 1, max: 50 }).withMessage("Name must be 1 to 50 characters long"),

	check("username")

		.not().isEmpty({ignore_whitespace: true}).withMessage("Please enter a username")
		.trim()
		.isLength({min: 3, max: 15}).withMessage("Username must be 3 to 15 characters long"),

	check("email")
	
		.not().isEmpty({ ignore_whitespace: true }).withMessage("Please enter an email address")
		.isEmail().withMessage("Please enter a valid email address")
		.normalizeEmail(),

	check("password")
	
		.not().isEmpty({ ignore_whitespace: true }).withMessage("Please enter a password")
		.isLength({ min: 8 }).withMessage("Password must be at least 8 characters long")

], (req, res) => {

	const errors = validationResult(req);

	if (!errors.isEmpty()) {

		const mapped = errors.mapped();

		const name = req.body.name;
		const username = req.body.username;
		const email = req.body.email;

		const nameError = mapped.name ? mapped.name.msg : "";
		const usernameError = mapped.username ? mapped.username.msg : "";
		const emailError = mapped.email ? mapped.email.msg : "";
		const passwordError = mapped.password ? mapped.password.msg : "";

		res.render("register", { title: "Register", name: name, username: username, email: email, nameError: nameError, usernameError: usernameError, emailError: emailError, passwordError: passwordError });

	} else {

		req.flash("success", "Your account has been created.");

		res.redirect("/login");

	}

});

app.get("*", (req, res) => {

	res.render("404.pug", { title: "404", path: req.path.replace(/\/$/, "").replace(/\/|%20/g, "+") });

});

app.use((err, req, res, next) => {

	res.render("error.pug", {title: "Error", error: err.stack});

 });

app.listen(3000);